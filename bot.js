const Discord = require('discord.io');
const { Rcon } = require('rcon-client');
const fs = require('fs');
const { token, adminIds, discordReconnectTimeout, servers } = require('./config.json');
const logger = require('./logger.js');
const serverNames = Object.keys(servers);

let bot = new Discord.Client({
    token
});

const connect = () => {
    bot.connect();
};

connect();

bot.on('disconnect', function () {
    logger.info('Disconnected');
    logger.info('Trying to reconnect in 10s');
    setTimeout(connect, discordReconnectTimeout);
});

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

bot.on('message', function (user, userID, channelID, message, evt) {

    const embedImage = (url) => {
        bot.sendMessage({
            to: channelID,
            embed: {
                image: {
                    url
                }
            }
        });
    };

    const embed = (title, description) => {
        bot.sendMessage({
            to: channelID,
            embed: {
                title, description
            }
        });
    };

    const singleRcon = (serverName, cmd) => {
        Rcon.connect(servers[serverName]).then(rcon => {
            rcon.send(cmd).then((res) => {
                embed(serverName, res);
                rcon.end()
            });
        });
    };

    const multipleRcon = (cmd) => {

        let promises = serverNames.map(s => {
            return new Promise((resolve, reject) => {
                Rcon.connect(servers[s]).then(rcon => {
                    rcon.send(cmd).then((res) => resolve({ s, res }), res => resolve({ s, res })).then(() => rcon.end()).catch(res => resolve({ s, res }));
                }, res => resolve({ s, res}));
            });
        });

        Promise.all(promises).then(results => {
            logger.info(results);
            let output = '';
            results.forEach(result => {
                output += `**${result.s}** \n ${result.res} \n\n`;
            });
            embed(cmd, output);
        })

    };

    const usage = () => embed('Usage', fs.readFileSync('./usage.txt') + [""].concat(Object.keys(servers)).join('\n - '));

    if (message.substring(0, 1) == '!') {

        if (adminIds.indexOf(userID) === -1) {
            reply("Sorry you're not allowed to use that bot");
            return;
        }

        var args = message.substring(1).split(' ');
        var cmd = args[0];

        let serverName = args[0];
        let cmdLine = args.splice(1, args.length - 1).join(' ');

        if (serverNames.indexOf(serverName) !== -1) {
            singleRcon(serverName, cmdLine);
        }
        else if ('all' === serverName) {
            multipleRcon(cmdLine);
        }
        else if ('complain' === serverName) {
            embedImage('https://i.imgflip.com/3wt8wz.jpg');
        }
        else if ('moan' === serverName) {
            embedImage('https://media1.tenor.com/images/cda1c3d407a02069493154ee50fec824/tenor.gif?itemid=9982157');
        }
        else if ('help' === serverName) {
            usage();
        }
    }
});

